# card10

Apps and stuff for the card10 badge from Chaos Communication Camp 2019.

## Uhrwerk

Swiss-style clockwork animation

![Uhrwerk](./pictures/uhrwerk.jpg)

Features:
- displays the current time
- realistic gear movement
- spring position indicates the battery charge

Removed features:
- drawing of gear teeth (cogs) (used too many resources)

Known issues:
- circles are drawn incompletely since a later card10 firmware release

There are some [demo videos](https://verbuecheln.ch/card10/).

### Micropython version

The Micropython version can be installed from the [hatchery](https://hatchery.badge.team/projects/uhrwerk).

### C version (l0dable)

To install the l0dable, follow the following steps:

- activate l0dables in `card10.cfg` by adding the line `execute_elf = true`
- add the binary “[uhrwerk.elf](https://verbuecheln.ch/card10/uhrwerk.elf)” ([PGP signature](https://verbuecheln.ch/card10/uhrwerk.elf.sig)) to the app directory

## Diapositiv

Photo gallery app for your card10.

![Diapositiv](./pictures/diapositiv.jpg)

### How it works

To install and run the l0dable, follow the following steps:

- activate l0dables in `card10.cfg` by adding the line `execute_elf = true`
- add the binary “[diapositiv.elf](https://verbuecheln.ch/card10/diapositiv.elf)” ([PGP signature](https://verbuecheln.ch/card10/diapositiv.elf.sig)) to the app directory
- create a directory `/gallery/` in USB storage mode
- place any card10 framebuffer images there

Tip: You can try with the example images in the gallery directory of this repository.

### Convert images

The card10 framebuffer uses a format with 16 bits per RGB pixel. It uses 5 bits for red, 6 bits for green and 5 bits for blue. With its 160×80 pixels, a photo has a total size of 25600 bytes.

For this small picture format, compression does not gain much for photos and will just waste time and battery. So I decided to convert photos to framebuffer images before copying them to the card10.

You can convert any photos to card10 framebuffer images using the `convert.py` script. It will resize and crop the photo to 160x80 and convert the color space. For a JPEG image named `photo.jpg`, it will create a card10 framebuffer image named `photo.cfb`. The file extension is used for convenience and does not have an effect.

    $ ./convert.py photo.jpg

The script requires ImageMagick.

## Power Stats

Displays voltage, current, power and battery percentage (estimate).

## Compass

Simple compass app.

## Build instructions for l0dables

The easiest way to build a l0dable is to checkout the firmware source tree and place the app there. It will take care of all the headers and linking.

- Checkout firmware source from [Git](https://git.card10.badge.events.ccc.de/card10/firmware)
- Install the required dependencies
- Copy the directory `l0dables/uhrwerk` to `firmware/l0dables/uhrwerk`
- Add the subdirectory to `firmware/l0dables/meson.build`
- Follow the build instructions for firmware
- find the binary in `firmware/build/l0dables/uhrwerk/`
