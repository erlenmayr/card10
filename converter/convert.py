#!/usr/bin/python3

# transforms 24-bit bitmaps into 16-bit (RGB565) framebuffer images for card10

import os
import sys

filename = sys.argv[1]

# use ImageMagick (required) to decode and resize
os.system('convert "' + filename + '" -resize 160x160 -flop -gravity center -extent 160x80 "' + filename + '.bmp"')

# convert to card10 framebuffer color space
with open(filename + '.cfb', 'wb') as card10:
    with open(filename + '.bmp', 'rb') as bitmap:
        bitmap.seek(0x8a);
        for i in range(160 * 80):
            pixel24 = bitmap.read(3)
            r = pixel24[2] >> 3;            
            g = pixel24[1] >> 2;            
            b = pixel24[0] >> 3;
            pixel16 = r << 11 | g << 5 | b
            card10.write(pixel16.to_bytes(2, 'big'))

