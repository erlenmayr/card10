/*
 * Diapositiv
 *
 * Photo gallery app for your card10
 *
 * Author: Erlenmayr
 * Contact: verbuecheln@posteo.de
 * Source: https://gitlab.com/erlenmayr/card10
 * License: GPLv2
 */

#include "epicardium.h"
#include "string.h"

#define BLACK 0x0000
#define RED 0xf800
#define YELLOW 0xffe0

#define FB_SIZE 25600

union disp_framebuffer fb;



void disp_error(const char *title, const char *msg, uint16_t color) {
     epic_disp_print(8, 20, title, color, BLACK);
     epic_disp_print(8, 40, msg, color, BLACK);
     epic_disp_update();
     epic_sleep(5000);
     epic_exit(-1);
}



int main(void) {
    epic_disp_open();
    epic_disp_clear(BLACK);

    /* open the gallery directory */
    char path[512] = "/gallery";
    int gallery = epic_file_opendir(path);
    if (gallery < 0)
        disp_error("Could not", "open gallery.", RED);

    /* check the directory for files (EPICSTAT_NONE means end of list) */
    struct epic_stat entry;
    epic_file_readdir(gallery, &entry);
    if (entry.type == EPICSTAT_NONE)
        disp_error("No photos", "in gallery.", RED);

    while (true) {
        /* if possible (i.e. entry is not a directory), display the file */
        if (entry.type == EPICSTAT_FILE) {
            path[8] = '/';
            strcpy(path + 9, entry.name);
            int photo = epic_file_open(path, "r");
            if (epic_file_read(photo, &fb, FB_SIZE) == FB_SIZE) {
                epic_disp_framebuffer(&fb);
            } else {
                disp_error("Invalid size:", entry.name, YELLOW);
            }
            epic_file_close(photo);
        } else {
            disp_error("Skipping directory:", entry.name, YELLOW);
        }

        for (int timeout = 10000; timeout > 0; timeout -= 250) {
            if (timeout < 10000 && epic_buttons_read(BUTTON_RIGHT_TOP))
                break;
            epic_sleep(250);
        }

        /* get the next directory entry, reset in case of last file */
        epic_file_readdir(gallery, &entry);
        if (entry.type == EPICSTAT_NONE) {
            epic_file_readdir(gallery, NULL);
            epic_file_readdir(gallery, &entry);
        }
    }

    epic_file_close(gallery);
}
