/*
 * Uhrwerk
 *
 * Swiss-style clockwork animation
 * 
 * Author: Erlenmayr
 * Contact: verbuecheln@posteo.de
 * Source: https://gitlab.com/erlenmayr/card10
 * License: GPLv2
 */

#include "epicardium.h"

#include <math.h>
#include <stdio.h>

#define PI 3.14159265

// TODO: Read timezone from config file
#define TIMEZONE 2

/*
 * this macro converts 24-bit RGB into 16-bit RGB as used by card10 (5-6-5 bits)
 */
#define COLOR(R, G, B) ((((R) >> 3) << 11) | (((G) >> 2) << 5) | ((B) >> 3))

typedef enum {
    BLACK     = COLOR(  0,   0,   0),
    RED       = COLOR(128,   0,   0),
    GREEN     = COLOR(  0, 128,   0),
    BLUE      = COLOR(  0,   0, 255),
    ORANGERED = COLOR(255,  69,   0),
    GOLD      = COLOR(255, 215,   0),
    GRAY      = COLOR( 64,  64,  64),
    REDGRAY   = COLOR( 80,  64,  64),
    GREENGRAY = COLOR( 64,  80,  64),
    BLUEGRAY  = COLOR( 64,  64,  80),
    DARKGOLD  = COLOR(128,  96,  16),
} color;



void draw_spring(float charge) {
    int16_t pos = (90 - 60 * charge);

    epic_disp_rect(10, 5, 99, 14, RED, FILLSTYLE_FILLED, 1);
    epic_disp_rect(100, 5, 159, 14, GREEN, FILLSTYLE_FILLED, 1);
    epic_disp_rect(0, 0, 9, 20, GRAY, FILLSTYLE_FILLED, 1);

    epic_disp_rect(11 + pos, 1, 79 + pos, 19, GRAY, FILLSTYLE_FILLED, 1);

    for (int i = 0; i < 6; i++) {
        int16_t f = 10 + ((2 * i + 1) * (pos / 12.0));
        int16_t g = 10 + ((2 * i + 2) * (pos / 12.0));
        epic_disp_line(f, 2, g, 18, GRAY, LINESTYLE_FULL, 2);
    }
}



void draw_gear(int16_t x, int16_t y, int16_t radius, float ang, color c) {
    int16_t xd = radius * sinf(ang);
    int16_t yd = radius * cosf(ang);
    epic_disp_circ(x, y, radius, c, FILLSTYLE_EMPTY, 5);
    epic_disp_line(x + xd, y + yd, x - xd, y - yd, c, LINESTYLE_FULL, 5);
}



void draw_gears(uint32_t min, uint32_t s, uint32_t ms) {
    // flywheel
    draw_gear(90, 39, 48, (PI / 2) - (PI / 3) * sinf((PI / 500) * ms), DARKGOLD);
    // minute gears
    draw_gear(80, 40, 35, min * -(PI / 30), GREENGRAY);
    draw_gear(133, 50, 15, min * (PI / 15), REDGRAY);
    // second gears
    draw_gear(80, 40, 15, s * -(PI / 30), GRAY);
    draw_gear(37, 46, 25, s * (PI / 60), BLUEGRAY);
}



void draw_hand(int16_t x, int16_t y, int16_t len, float ang, color c, uint16_t thick) {
    int16_t xs = x - len * sinf(ang);
    int16_t ys = y - len * cosf(ang);
    epic_disp_line(x, y, xs, ys, BLACK, LINESTYLE_FULL, thick + 2);
    epic_disp_line(x, y, xs, ys, c, LINESTYLE_FULL, thick);
}



void draw_hands(uint32_t h, uint32_t min, uint32_t s) {
    draw_hand(80, 40, 25, (h + min / 60.0) * -(PI / 6), GOLD, 3);
    draw_hand(80, 40, 35, min * -(PI / 30), GOLD, 3);
    draw_hand(80, 40, 40, s * -(PI / 30), ORANGERED, 2);
    epic_disp_circ(80, 40, 4, BLACK, 1, 1);
    epic_disp_circ(80, 40, 3, ORANGERED, 1, 1);
}



int main(void) {
    int err = epic_disp_open();
    if (!err) {
        // current time and old time (from last iteration)
        uint32_t t = 0, t_old = 0;
        // position for the spring
        float charge = 0;
        for (;;) {
            epic_disp_clear(0);

            t_old = t;
            t = epic_rtc_get_seconds();

            // hour, minute, second, millisecond
            uint32_t h = (t % 43200) / 3600 + TIMEZONE;
            uint32_t min = (t % 3600) / 60;
            uint32_t s = t % 60;
            uint32_t ms = epic_rtc_get_milliseconds() - (t * 1000ul);

            if (t != t_old) {
                float voltage = 0;
                epic_read_battery_voltage(&voltage);
                charge = (voltage - 3.4) / 0.8;
            }

            draw_spring(charge);
            draw_gears(min, s, ms);
            draw_hands(h, min, s);

            epic_disp_update();
        }
    }
    return err;
}

